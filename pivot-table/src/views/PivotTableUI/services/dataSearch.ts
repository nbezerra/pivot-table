import { createApi, fakeBaseQuery } from '@reduxjs/toolkit/query/react';
import { TableInput } from 'react-pivottable';

export const dataSearch = createApi({
  reducerPath: 'dataSearch',
  baseQuery: fakeBaseQuery(),
  endpoints: (builder) => ({
    getDataScript: builder.query<TableInput, void>({
      queryFn: () => {
        const data: TableInput = [
            {"Province": "Quebec", "Party": "NDP", "Age": "22", "Name": "Liu, Laurin", "Gender": "Female"},
            {"Province": "Quebec", "Party": "Bloc Quebecois", "Age": "43", "Name": "Mourani, Maria", "Gender": "Female"},
            {"Province": "Quebec", "Party": "NDP", "Age": "22", "Name": "Sellah, Djaouida", "Gender": "Male"},
        ];

        return {
          data
        };
      },
    }),
  }),
});

export const { useGetDataScriptQuery } =
  dataSearch;
