import PivotTableUI, { PivotTableUIProps } from 'react-pivottable/PivotTableUI';
import { useCallback, useState } from 'react';
import { StyledPivotTable } from './styles';
import useDataSearch from './hooks/useDataSearch';
import { TableInput } from 'react-pivottable';

const PivotTable = () => {
    const [props, setProps] = useState<PivotTableUIProps>();

    const { data } = useDataSearch();

    const defaultData: TableInput = [];

    const handleChange = useCallback((props: PivotTableUIProps) => {
        setProps(props);
    }, [])

    return (
        <StyledPivotTable>
            <PivotTableUI
                data={data || defaultData}
                onChange={(s) => handleChange(s)}
                {...props}
            />
        </StyledPivotTable>
    )
}

export default PivotTable;