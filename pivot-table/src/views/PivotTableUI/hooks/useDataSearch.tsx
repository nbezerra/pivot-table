import { useGetDataScriptQuery } from '../services/dataSearch';

const useDataSearch = () => {
    const { data } = useGetDataScriptQuery();

    return { data };
};

export default useDataSearch;