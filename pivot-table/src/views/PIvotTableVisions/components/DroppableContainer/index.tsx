import { Droppable } from "react-beautiful-dnd";

const DroppableContainer = ({ name, children }:  { name: string, children: any }) => {
    return (
        <Droppable droppableId={name}>
            {(provided, snapshot) => (
                <div ref={provided.innerRef}>
                    {children}
                    {provided.placeholder}
                </div>
            )}
        </Droppable>
    );
};

export default DroppableContainer;