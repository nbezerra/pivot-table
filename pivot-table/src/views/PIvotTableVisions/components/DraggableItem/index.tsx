import { Draggable } from "react-beautiful-dnd";

const DraggableItem = ({ item, index }: { item: any, index: number }) => {
    return (
        <Draggable key={item.id} draggableId={`${item.id}`} index={index}>
            {(
                provided,
                snapshot
            ) => (
                <div>
                <div
                    ref={provided.innerRef}
                    {...provided.draggableProps}
                    {...provided.dragHandleProps}
                >
                    {item.title}
                </div>
                </div>
            )}
        </Draggable>
    );
};

export default DraggableItem;