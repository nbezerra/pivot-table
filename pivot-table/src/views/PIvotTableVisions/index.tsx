import { Box } from "@mui/material";
import { useState } from "react";
import { DragDropContext } from "react-beautiful-dnd";
import DraggableItem from "./components/DraggableItem";
import DroppableContainer from "./components/DroppableContainer";
import itemsNormal from "./mocks";

const PivotTableVisions = () => {
    const [items, setItems] = useState(itemsNormal);

    const removeFromList = (list: any, index: any) => {
        const result = Array.from(list);
        const [removed] = result.splice(index, 1);
        return [removed, result];
      };
    
      const addToList = (list: any, index: any, element: any) => {
        const result = Array.from(list);
        result.splice(index, 0, element);
        return result;
      };
    
      const onDragEnd = (result: any) => {
        if (!result.destination) {
            console.log(result);
            return;
        }
        const listCopy: any = { ...items };
        const sourceList = listCopy[result.source.droppableId];
        const [removedElement, newSourceList] = removeFromList(
        sourceList,
        result.source.index
        );
        listCopy[result.source.droppableId] = newSourceList;
    
        const destinationList = listCopy[result.destination.droppableId];
        listCopy[result.destination.droppableId] = addToList(
        destinationList,
        result.destination.index,
        removedElement
        );
        setItems(listCopy);
    };
    
    return (
        <DragDropContext onDragEnd={onDragEnd}>
            <Box display={'flex'}>
                <Box margin={'10px'} width={'300px'} height={'300px'}>
                    <DroppableContainer name={'lines'}>
                        {items.lines.map((item: any, index: number) => (
                            <DraggableItem item={item} index={index} />
                        ))}
                    </DroppableContainer>
                </Box>
                <Box margin={'10px'} width={'300px'} height={'300px'}>
                    <DroppableContainer name={'columns'}>
                        {items.columns.map((item: any, index: number) => (
                            <DraggableItem item={item} index={index} />
                        ))}
                    </DroppableContainer>
                </Box>
            </Box>
        </DragDropContext>
    );
};

export default PivotTableVisions;