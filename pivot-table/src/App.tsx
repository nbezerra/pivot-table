import { Provider } from 'react-redux';
import { store } from './store';
import PivotTableVisions from './views/PIvotTableVisions';

function App() {

  return (
    <Provider store={store}>
      <PivotTableVisions />
    </Provider>
  )
}

export default App;
