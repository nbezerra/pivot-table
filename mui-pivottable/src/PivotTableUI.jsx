import React from 'react';
import PropTypes from 'prop-types';
import update from 'immutability-helper';
import {PivotData, sortAs, getSort} from './utils/Utilities';
import PivotTable from './components/PivotTable';
import Sortable from 'react-sortablejs';
import Draggable from 'react-draggable';
import {
    Box,
    Button,
    Link,
    MenuItem,
    Select,
    TableBody,
    TableCell,
    TableRow,
    TextField,
    Typography} from '@mui/material';
import {StyledPivotTable} from './styles/styledPivottable';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import CloseIcon from '@mui/icons-material/Close';
import MenuIcon from '@mui/icons-material/Menu';

/* eslint-disable react/prop-types */
// eslint can't see inherited propTypes!

class DraggableAttribute extends React.Component {
    constructor(props) {
        super(props);
        this.state = {open: false, top: 0, left: 0, filterText: ''};
    }

    toggleValue(value) {
        if ((value in this.props.valueFilter)) {
            this.props.removeValuesFromFilter(this.props.name, [value]);
        }
        else {
            this.props.addValuesToFilter(this.props.name, [value]);
        }
    }

    matchesFilter(x) {
        return x.toLowerCase().trim().includes(this.state.filterText.toLowerCase().trim());
    }

    selectOnly(e, value) {
        e.stopPropagation();
        this.props.setValuesInFilter(this.props.name,
            Object.keys(this.props.attrValues).filter(y => y !== value));
    }

    getFilterBox() {
        const showMenu = Object.keys(this.props.attrValues).length < this.props.menuLimit;

        const values = Object.keys(this.props.attrValues);
        const shown = values.filter(this.matchesFilter.bind(this))
            .sort(this.props.sorter);

        return (
            <Draggable handle=".pvtDragHandle">
                <Box className="pvtFilterBox" sx={{
                    display: 'block', cursor: 'initial', zIndex: this.props.zIndex,
                    top: this.state.top + 'px', left: this.state.left + 'px', borderRadius: '4px',
                    padding: '5px'
                }}
                onClick={() => this.props.moveFilterBoxToTop(this.props.name)}
                >
                    <Box margin={'5px'}>
                        <Link onClick={() => this.setState({open: false})}
                            className="pvtCloseX"
                        >
                            <CloseIcon />
                        </Link>
                        <Typography className="pvtDragHandle">
                            <MenuIcon />
                        </Typography>
                        <Typography fontSize={'14px'}>{this.props.name}</Typography>

                        {showMenu ||
                        <Typography>(too many values to show)</Typography>
                        }
                    </Box>

                    {showMenu &&
                    <Box>
                        <TextField
                            variant={'outlined'}
                            fullWidth
                            type={'text'}
                            placeholder="Filtrar valores"
                            className="pvtSearch"
                            value={this.state.filterText}
                            onChange={e => this.setState({filterText: e.target.value})}
                            size={'small'}
                        />
                        <Box margin={'10px'}>
                            <Button className="pvtButton"
                                onClick={() => this.props.removeValuesFromFilter(this.props.name,
                                    Object.keys(this.props.attrValues).filter(this.matchesFilter.bind(this)))}
                            >
                            Select {values.length === shown.length ? 'All' : shown.length}
                            </Button> <Button className="pvtButton"
                                onClick={() => this.props.addValuesToFilter(this.props.name,
                                    Object.keys(this.props.attrValues).filter(this.matchesFilter.bind(this)))}
                            >
                            Deselect {values.length === shown.length ? 'All' : shown.length}
                            </Button>
                        </Box>
                    </Box>
                    }

                    {showMenu &&
                    <Box className="pvtCheckContainer">
                        {shown.map(x =>
                            <Typography key={x} onClick={() => this.toggleValue(x)}
                                className={(x in this.props.valueFilter) ? '' : 'selected'}
                                fontSize={'14px'}
                            >
                                <Link className="pvtOnly"
                                    onClick={e => this.selectOnly(e, x)}
                                >only</Link>
                                <Link className="pvtOnlySpacer">&nbsp;</Link>

                                {x === '' ? <em>null</em> : x}

                            </Typography>)}
                    </Box>
                    }
                </Box>
            </Draggable>);
    }

    toggleFilterBox(event) {
        const bodyRect = document.body.getBoundingClientRect();
        const rect = event.nativeEvent.target.getBoundingClientRect();
        this.setState({
            open: !this.state.open,
            top: 10 + rect.top - bodyRect.top,
            left: 10 + rect.left - bodyRect.left});
        this.props.moveFilterBoxToTop(this.props.name);
    }

    render() {
        const filtered = Object.keys(this.props.valueFilter).length !== 0 ? 'pvtFilteredAttribute' : '';
        return <li data-id={this.props.name}>
            <Box component={'span'}
                className={'pvtAttr ' + filtered}
                sx={{
                    backgroundColor: 'white',
                    border: '1px solid #cccccc',
                    borderRadius: '4px',
                    padding: '2px 4px'
                }}
            >
                <Typography component={'span'} fontSize={'14px'}>{this.props.name}</Typography>
                <Typography component={'span'}
                    className="pvtTriangle"
                    onClick={this.toggleFilterBox.bind(this)}
                >
                    <KeyboardArrowDownIcon fontSize="medium" />
                </Typography>
            </Box>

            {this.state.open ? this.getFilterBox() : null}
        </li>;
    }
}


DraggableAttribute.defaultProps = {
    valueFilter: {}
};

DraggableAttribute.propTypes = {
    name: PropTypes.string.isRequired,
    addValuesToFilter: PropTypes.func.isRequired,
    removeValuesFromFilter: PropTypes.func.isRequired,
    attrValues: PropTypes.objectOf(PropTypes.number).isRequired,
    valueFilter: PropTypes.objectOf(PropTypes.bool),
    moveFilterBoxToTop: PropTypes.func.isRequired,
    sorter: PropTypes.func.isRequired,
    menuLimit: PropTypes.number,
    zIndex: PropTypes.number
};

class Dropdown extends React.PureComponent {

    render() {
        return <Box className="pvtDropdown" sx={{zIndex: this.props.zIndex, width: '100%'}}>
            <Select sx={{fontSize: '14px'}} fullWidth variant={'standard'} value={this.props.current}>
                {this.props.values.map(r => <MenuItem sx={{fontSize: '14px'}} value={r} key={r} role="button"
                    onClick={e => {
                        e.stopPropagation();
                        if (this.props.current === r) {
                            this.props.toggle();
                        }
                        else { this.props.setValue(r); }
                    }}
                    className={'pvtDropdownValue ' +
                                (r === this.props.current ? 'pvtDropdownActiveValue' : '')}
                >{r}</MenuItem>
                )}
            </Select>

        </Box>;
    }
}


class PivotTableUI extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            unusedOrder: [], zIndices: {}, maxZIndex: 1000,
            openDropdown: false
        };
    }

    componentWillMount() {
        this.materializeInput(this.props.data);
    }

    componentWillUpdate(nextProps) {
        this.materializeInput(nextProps.data);
    }

    materializeInput(nextData) {
        if (this.data === nextData) { return; }
        this.data = nextData;
        const attrValues = {};
        const materializedInput = [];
        let recordsProcessed = 0;
        PivotData.forEachRecord(this.data, this.props.derivedAttributes, function(record) {
            materializedInput.push(record);
            for (const attr of Object.keys(record)) {
                if (!(attr in attrValues)) {
                    attrValues[attr] = {};
                    if (recordsProcessed > 0) {
                        attrValues[attr].null = recordsProcessed;
                    }
                }
            }
            for (const attr in attrValues) {
                const value = attr in record ? record[attr] : 'null';
                if (!(value in attrValues[attr])) { attrValues[attr][value] = 0; }
                attrValues[attr][value]++;
            }
            recordsProcessed++;
        });

        this.materializedInput = materializedInput;
        this.attrValues = attrValues;
    }

    sendPropUpdate(command) {
        this.props.onChange(update(this.props, command));
    }

    propUpdater(key) {
        return value => this.sendPropUpdate({[key]: {$set: value}});
    }

    setValuesInFilter(attribute, values) {
        this.sendPropUpdate({valueFilter: {[attribute]:
                {$set: values.reduce((r, v) => { r[v] = true; return r; }, {})}
        }});
    }

    addValuesToFilter(attribute, values) {
        if (attribute in this.props.valueFilter) {
            this.sendPropUpdate({valueFilter: {[attribute]:
                values.reduce((r, v) => { r[v] = {$set: true}; return r; }, {})
            }});
        }
        else {
            this.setValuesInFilter(attribute, values);
        }
    }

    removeValuesFromFilter(attribute, values) {
        this.sendPropUpdate({valueFilter: {[attribute]: {$unset: values}}});
    }

    moveFilterBoxToTop(attribute) {
        this.setState(update(this.state, {
            maxZIndex: {$set: this.state.maxZIndex + 1},
            zIndices: {[attribute]: {$set: this.state.maxZIndex + 1}}
        }));
    }

    isOpen(dropdown) {
        return this.state.openDropdown === dropdown;
    }

    makeDnDCell(items, onChange, classes) {
        return <Sortable
            options={{
                group: 'shared', ghostClass: 'pvtPlaceholder',
                filter: '.pvtFilterBox', preventOnFilter: false
            }}
            tag="td" className={classes} onChange={onChange}
        >
            {items.map(x => <DraggableAttribute name={x} key={x}
                attrValues={this.attrValues[x]}
                valueFilter={this.props.valueFilter[x] || {}}
                sorter={getSort(this.props.sorters, x)}
                menuLimit={this.props.menuLimit}
                setValuesInFilter={this.setValuesInFilter.bind(this)}
                addValuesToFilter={this.addValuesToFilter.bind(this)}
                moveFilterBoxToTop={this.moveFilterBoxToTop.bind(this)}
                removeValuesFromFilter={this.removeValuesFromFilter.bind(this)}
                zIndex={this.state.zIndices[x] || this.state.maxZIndex}
            />)}
        </Sortable>;
    }

    render() {
        const numValsAllowed = this.props.aggregators[this.props.aggregatorName]([])().numInputs || 0;

        const rendererName = this.props.rendererName in this.props.renderers ?
            this.props.rendererName : Object.keys(this.props.renderers)[0];

        const rendererCell = <TableCell className="pvtRenderers">
            <Typography fontSize={'14px'} color={'#666'}>Visão</Typography>
            <Dropdown
                current={rendererName}
                values={Object.keys(this.props.renderers)}
                open={this.isOpen('renderer')}
                zIndex={this.isOpen('renderer') ? this.state.maxZIndex + 1 : 1}
                toggle={() => this.setState({openDropdown:
                     this.isOpen('renderer') ? false : 'renderer'})}
                setValue={this.propUpdater('rendererName')}
            />
        </TableCell>;

        const aggregatorCell = <TableCell className="pvtVals">
            <Typography fontSize={'14px'} color={'#666'}>Totais</Typography>
            <Dropdown
                current={this.props.aggregatorName}
                values={Object.keys(this.props.aggregators)}
                open={this.isOpen('aggregators')}
                zIndex={this.isOpen('aggregators') ? this.state.maxZIndex + 1 : 1}
                toggle={() => this.setState({openDropdown:
                    this.isOpen('aggregators') ? false : 'aggregators'})}
                setValue={this.propUpdater('aggregatorName')}
            />
            {(numValsAllowed > 0) && <br />}
            {new Array(numValsAllowed).fill().map((n, i) =>
                [
                    <Dropdown
                        current={this.props.vals[i]}
                        values={Object.keys(this.attrValues).filter(e =>
                            !this.props.hiddenAttributes.includes(e) &&
                        !this.props.hiddenFromAggregators.includes(e))}
                        open={this.isOpen(`val${i}`)}
                        zIndex={this.isOpen(`val${i}`) ? this.state.maxZIndex + 1 : 1}
                        toggle={() => this.setState({openDropdown:
                            this.isOpen(`val${i}`) ? false : `val${i}`})}
                        setValue={value =>
                            this.sendPropUpdate({vals: {$splice: [[i, 1, value]]}})}
                    />,
                    i + 1 !== numValsAllowed ? <br /> : null
                ]
            )}
        </TableCell>;

        const unusedAttrs = Object.keys(this.attrValues)
            .filter(e => !this.props.rows.includes(e) &&
                    !this.props.cols.includes(e) &&
                    !this.props.hiddenAttributes.includes(e) &&
                    !this.props.hiddenFromDragDrop.includes(e))
            .sort(sortAs(this.state.unusedOrder));

        const unusedLength = unusedAttrs.reduce(((r, e) => r + e.length), 0);
        const horizUnused = unusedLength < this.props.unusedOrientationCutoff;

        const unusedAttrsCell = this.makeDnDCell(unusedAttrs, (order => this.setState({unusedOrder: order})),
            `pvtAxisContainer pvtUnused ${horizUnused ? 'pvtHorizList' : 'pvtVertList'}`);

        const colAttrs = this.props.cols.filter(e =>
            !this.props.hiddenAttributes.includes(e) &&
                    !this.props.hiddenFromDragDrop.includes(e));

        const colAttrsCell = this.makeDnDCell(colAttrs, this.propUpdater('cols'),
            'pvtAxisContainer pvtHorizList pvtCols');

        const rowAttrs = this.props.rows.filter(e =>
            !this.props.hiddenAttributes.includes(e) &&
                    !this.props.hiddenFromDragDrop.includes(e));
        const rowAttrsCell = this.makeDnDCell(rowAttrs, this.propUpdater('rows'),
            'pvtAxisContainer pvtVertList pvtRows');
        const outputCell = <TableCell className="pvtOutput">
            <PivotTable {...update(this.props, {data: {$set: this.materializedInput}})} />
        </TableCell>;

        if (horizUnused) {
            return <StyledPivotTable><TableBody
                onClick={() => this.setState({openDropdown: false})}
            >
                <TableRow
                    sx={{backgroundColor: '#333', textAlign: 'center'}}
                >
                    <TableCell colSpan={2}>
                        <Typography fontSize={'18px'} color={'#fff'}
                            sx={{textAlign: 'center'}}
                        >
                            VISÃO DE GRAVAÇÃO DOS ROTEIROS
                        </Typography>
                    </TableCell>
                </TableRow>
                <TableRow>{rendererCell }{ unusedAttrsCell }</TableRow>
                <TableRow>{aggregatorCell }{ colAttrsCell }</TableRow>
                <TableRow>{rowAttrsCell }{ outputCell }</TableRow>
            </TableBody></StyledPivotTable>;
        }

        return <StyledPivotTable className="pvtUi"><TableBody
            onClick={() => this.setState({openDropdown: false})}
        >
            <TableRow>{rendererCell }{ aggregatorCell }{ colAttrsCell }</TableRow>
            <TableRow>{unusedAttrsCell }{ rowAttrsCell }{ outputCell }</TableRow>
        </TableBody></StyledPivotTable>;
    }
}

PivotTableUI.propTypes = Object.assign({}, PivotTable.propTypes, {
    onChange: PropTypes.func.isRequired,
    hiddenAttributes: PropTypes.arrayOf(PropTypes.string),
    hiddenFromAggregators: PropTypes.arrayOf(PropTypes.string),
    hiddenFromDragDrop: PropTypes.arrayOf(PropTypes.string),
    unusedOrientationCutoff: PropTypes.number,
    menuLimit: PropTypes.number
});

PivotTableUI.defaultProps = Object.assign({}, PivotTable.defaultProps, {
    hiddenAttributes: [],
    hiddenFromAggregators: [],
    hiddenFromDragDrop: [],
    unusedOrientationCutoff: 85,
    menuLimit: 500
});

export default PivotTableUI;
