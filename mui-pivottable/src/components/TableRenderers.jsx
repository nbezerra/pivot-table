import React from 'react';
import PropTypes from 'prop-types';
import {PivotData} from '../utils/Utilities';
import {Table, TableBody, TableCell, TableHead, TableRow} from '@mui/material';

const TableCellProps = {
    fontSize: '12px',
    '&.MuiTableCell-head': {
        backgroundColor: '#eaeaea',
        fontWeight: 'bold',
        border: '1px solid #fff'
    },
    '&.pvtRowLabel': {
        backgroundColor: '#f5f5f5',
        border: '1px solid #fff'
    }
};

// helper function for setting row/col-span in pivotTableRenderer
const spanSize = function(arr, i, j) {
    let x;
    if (i !== 0) {
        let asc, end;
        let noDraw = true;
        for (x = 0, end = j, asc = end >= 0; asc ? x <= end : x >= end; asc ? x++ : x--) {
            if (arr[i - 1][x] !== arr[i][x]) {
                noDraw = false;
            }
        }
        if (noDraw) {
            return -1;
        }
    }
    let len = 0;
    while ((i + len) < arr.length) {
        let asc1, end1;
        let stop = false;
        for (x = 0, end1 = j, asc1 = end1 >= 0; asc1 ? x <= end1 : x >= end1; asc1 ? x++ : x--) {
            if (arr[i][x] !== arr[i + len][x]) { stop = true; }
        }
        if (stop) { break; }
        len++;
    }
    return len;
};

function redColorScaleGenerator(values) {
    const min = Math.min.apply(Math, values);
    const max = Math.max.apply(Math, values);
    return x => {
        // eslint-disable-next-line no-magic-numbers
        const nonRed = 255 - Math.round(255 * (x - min) / (max - min));
        return {backgroundColor: `rgb(255,${nonRed},${nonRed})`};
    };
}

function makeRenderer(opts = {}) {

    class TableRenderer extends React.PureComponent {
        render() {
            const pivotData = new PivotData(this.props);
            const colAttrs = pivotData.props.cols;
            const rowAttrs = pivotData.props.rows;
            const rowKeys = pivotData.getRowKeys();
            const colKeys = pivotData.getColKeys();
            const grandTotalAggregator = pivotData.getAggregator([], []);

            let valueCellColors = () => {};
            let rowTotalColors = () => {};
            let colTotalColors = () => {};
            if (opts.heatmapMode) {
                const colorScaleGenerator = this.props.tableColorScaleGenerator;
                const rowTotalValues = colKeys.map(x => pivotData.getAggregator([], x).value());
                rowTotalColors = colorScaleGenerator(rowTotalValues);
                const colTotalValues = rowKeys.map(x => pivotData.getAggregator(x, []).value());
                colTotalColors = colorScaleGenerator(colTotalValues);

                if (opts.heatmapMode === 'full') {
                    const allValues = [];
                    rowKeys.map(
                        r => colKeys.map(
                            c => allValues.push(
                                pivotData.getAggregator(r, c).value())));
                    const colorScale = colorScaleGenerator(allValues);
                    valueCellColors = (r, c, v) => colorScale(v);
                }
                else if (opts.heatmapMode === 'row') {
                    const rowColorScales = {};
                    rowKeys.map(r => {
                        const rowValues = colKeys.map(x => pivotData.getAggregator(r, x).value());
                        rowColorScales[r] = colorScaleGenerator(rowValues);
                    });
                    valueCellColors = (r, c, v) => rowColorScales[r](v);
                }
                else if (opts.heatmapMode === 'col') {
                    const colColorScales = {};
                    colKeys.map(c => {
                        const colValues = rowKeys.map(x => pivotData.getAggregator(x, c).value());
                        colColorScales[c] = colorScaleGenerator(colValues);
                    });
                    valueCellColors = (r, c, v) => colColorScales[c](v);
                }
            }

            return (
                <Table className="pvtTable">
                    <TableHead>
                        {colAttrs.map(function(c, j) { return (
                            <TableRow key={`colAttr${j}`}>
                                {(j === 0 && rowAttrs.length !== 0) &&
                                <TableCell
                                    sx={TableCellProps}
                                    variant="head"
                                    colSpan={rowAttrs.length}
                                    rowSpan={colAttrs.length}
                                />
                                }
                                <TableCell
                                    sx={TableCellProps}
                                    variant="head"
                                    className="pvtAxisLabel"
                                >{c.toUpperCase()}</TableCell>
                                {colKeys.map(function(colKey, i) {
                                    const x = spanSize(colKeys, i, j);
                                    if (x === -1) {return null;}
                                    return <TableCell
                                        sx={TableCellProps}
                                        variant="head"
                                        className="pvtColLabel"
                                        key={`colKey${i}`}
                                        colSpan={x} rowSpan={j === colAttrs.length - 1 && rowAttrs.length !== 0 ? 2 : 1}
                                    >
                                        {colKey[j]}
                                    </TableCell>;
                                })}

                                {(j === 0) &&
                                <TableCell sx={TableCellProps} variant="head" className="pvtTotalLabel"
                                    rowSpan={colAttrs.length + (rowAttrs.length === 0 ? 0 : 1)}
                                >Totais</TableCell>
                                }
                            </TableRow>
                        );})}

                        {(rowAttrs.length !== 0) &&
                        <TableRow>
                            {rowAttrs.map(function(r, i) {
                                return <TableCell sx={TableCellProps}
                                    variant="head"
                                    className="pvtAxisLabel"
                                    key={`rowAttr${i}`}
                                >{r.toUpperCase()}</TableCell>;
                            })}
                            <TableCell sx={TableCellProps}
                                variant="head"
                                className="pvtTotalLabel"
                            >{colAttrs.length === 0 ? 'Totais' : null}</TableCell>
                        </TableRow>
                        }
                    </TableHead>

                    <TableBody>
                        {rowKeys.map(function(rowKey, i) {
                            const totalAggregator = pivotData.getAggregator(rowKey, []);
                            return (
                                <TableRow key={`rowKeyRow${i}`}>
                                    {rowKey.map(function(txt, j) {
                                        const x = spanSize(rowKeys, i, j);
                                        if (x === -1) {return null;}
                                        return <TableCell sx={TableCellProps}
                                            variant="head"
                                            key={`rowKeyLabel${i}-${j}`}
                                            className="pvtRowLabel"
                                            rowSpan={x}
                                            colSpan={j === rowAttrs.length - 1 && colAttrs.length !== 0 ? 2 : 1}
                                        >{txt}</TableCell>;
                                    })}
                                    {colKeys.map(function(colKey, j) {
                                        const aggregator = pivotData.getAggregator(rowKey, colKey);
                                        return <TableCell
                                            sx={TableCellProps}
                                            className="pvtVal"
                                            key={`pvtVal${i}-${j}`}
                                            style={valueCellColors(rowKey, colKey, aggregator.value())}
                                        >
                                            {aggregator.format(aggregator.value())}</TableCell>;
                                    })}
                                    <TableCell sx={TableCellProps} className="pvtTotal"
                                        style={colTotalColors(totalAggregator.value())}
                                    >
                                        {totalAggregator.format(totalAggregator.value())}</TableCell>
                                </TableRow>
                            );
                        })}

                        <TableRow>
                            <TableCell sx={TableCellProps} variant="head" className="pvtTotalLabel"
                                colSpan={rowAttrs.length + (colAttrs.length === 0 ? 0 : 1)}
                            >Totais</TableCell>

                            {colKeys.map(function(colKey, i) {
                                const totalAggregator = pivotData.getAggregator([], colKey);
                                return <TableCell sx={TableCellProps} className="pvtTotal" key={`total${i}`}
                                    style={rowTotalColors(totalAggregator.value())}
                                >
                                    {totalAggregator.format(totalAggregator.value())}
                                </TableCell>;
                            })}

                            <TableCell sx={TableCellProps} className="pvtGrandTotal">
                                {grandTotalAggregator.format(grandTotalAggregator.value())}</TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
            );
        }
    }

    TableRenderer.defaultProps = PivotData.defaultProps;
    TableRenderer.propTypes = PivotData.propTypes;
    TableRenderer.defaultProps.tableColorScaleGenerator = redColorScaleGenerator;
    TableRenderer.propTypes.tableColorScaleGenerator = PropTypes.func;
    return TableRenderer;
}

class TSVExportRenderer extends React.PureComponent {
    render() {
        const pivotData = new PivotData(this.props);
        const rowKeys = pivotData.getRowKeys();
        const colKeys = pivotData.getColKeys();
        if (rowKeys.length === 0) { rowKeys.push([]); }
        if (colKeys.length === 0) { colKeys.push([]); }

        const headerRow = pivotData.props.rows.map(r => r);
        if (colKeys.length === 1 && colKeys[0].length === 0) {
            headerRow.push(this.props.aggregatorName);
        }
        else {
            colKeys.map(c => headerRow.push(c.join('-')));
        }

        const result = rowKeys.map(r => {
            const row = r.map(x => x);
            colKeys.map(c => {
                const v = pivotData.getAggregator(r, c).value();
                row.push(v ? v : '');
            });
            return row;
        });

        result.unshift(headerRow);

        return (<textarea value={result.map(r => r.join('\t')).join('\n')}
            style={{width: window.innerWidth / 2, height: window.innerHeight / 2}}
            readOnly={true}
        />);
    }
}

TSVExportRenderer.defaultProps = PivotData.defaultProps;
TSVExportRenderer.propTypes = PivotData.propTypes;

export default {
    'Tabela': makeRenderer(),
    'Mapa de Calor da Tabela': makeRenderer({heatmapMode: 'full'}),
    'Mapa de Calor da Coluna da Tabela': makeRenderer({heatmapMode: 'col'}),
    'Mapa de Calor da Linha da Tabela': makeRenderer({heatmapMode: 'row'}),
    'TSV Exportável': TSVExportRenderer
};
