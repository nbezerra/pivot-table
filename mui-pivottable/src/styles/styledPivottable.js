import {Table} from '@mui/material';
import styled from 'styled-components';

export const StyledPivotTable = styled(Table)`
    border-radius: 4px 4px 0 0;

    .pvtUi {
        color: #333;
    }

    .pvtOutput {
        padding: 0;
        border-bottom: none;
    }

    .pvtUnused {
        background-color: #eaeaea;
        border: 1px solid #f5f5f5;
    }
    .pvtUnused::before {
        content: 'Campos';
        font-weight: bold;
        padding: 10px;  
        text-align: center;
    }

    .pvtCols {
        background-color: white;
        border: 1px solid #f5f5f5;
    }
    .pvtCols::before {
        content: 'Colunas';
        font-weight: bold;
        padding: 10px;  
        text-align: center;
    }

    .pvtRows {
        background-color: white;
        border: 1px solid #f5f5f5;
        border-radius: 4px;
    }
    .pvtRows::before {
        content: 'Linhas';
        font-weight: bold;
        padding: 10px;  
        text-align: center;
    }


    .pvtRenderers {
        padding-left: 5px;
        user-select: none;
        width: 200px;
    }

    .pvtUi select {
        user-select: none;
        -webkit-user-select: none;
        -moz-user-select: none;
        -khtml-user-select: none;
        -ms-user-select: none;
    }
    .pvtAxisContainer, .pvtVals {
        padding: 5px;
        min-width: 20px;
        min-height: 20px;
    }

    .pvtDropdown {
        display: inline-block;
        position: relative;
        -webkit-user-select: none;
        -moz-user-select: none;
        -khtml-user-select: none;
        -ms-user-select: none;
        margin: 3px;
    }

    .pvtDropdownIcon {
        float: right;
        color: #A2B1C6;
    }
    .pvtDropdownCurrent {
        text-align: left;
        border: 1px solid #A2B1C6;
        display: inline-block;
        position: relative;
        width: 210px;
        box-sizing: border-box;
        background: white;
    }

    .pvtDropdownCurrentOpen {
        border-radius:  4px 4px 0 0;
    }

    .pvtDropdownMenu {
        background: white;
        position: absolute;
        width: 100%;
        margin-top: -1px;
        border-radius:  0 0 4px 4px;
        border: 1px solid  #A2B1C6;
        box-sizing: border-box;
    }

    .pvtDropdownValue {
        padding: 2px 5px;
        font-size: 12px;
        text-align: left;
    }
    .pvtDropdownActiveValue {
        background: #EBF0F8;
    }

    .pvtVals {
        white-space: nowrap;
        vertical-align: top;
        padding: 16px;
        padding-left: 5px;
    }

    .pvtRows { height: 35px; }

    .pvtAxisContainer li {
        padding: 8px 6px;
        list-style-type: none;
        cursor:move;
    }
    .pvtAxisContainer li.pvtPlaceholder {
        padding: 3px 15px;
        border: 1px dashed #A2B1C6;
    }
    .pvtAxisContainer li.pvtPlaceholder span.pvtAttr {
        display: none;
    }

    .pvtAxisContainer li span.pvtAttr {
        -webkit-text-size-adjust: 100%;
        white-space: nowrap;
        user-select: none;
        -webkit-user-select: none;
        -moz-user-select: none;
        -khtml-user-select: none;
        -ms-user-select: none;
    }

    .pvtTriangle {
        cursor: pointer;
        color: #333;
        align-self: center;
    }

    .pvtHorizList li { display: inline-block; }
    .pvtVertList { vertical-align: top; }

    .pvtFilteredAttribute { font-style: italic }

    .sortable-chosen .pvtFilterBox { display: none !important; }

    .pvtCloseX {
        position: absolute;
        right: 5px;
        top: 5px;
        font-size: 18px;
        cursor: pointer;
        text-decoration: none !important;
    }

    .pvtDragHandle {
        position: absolute;
        left: 5px;
        top: 5px;
        font-size: 18px;
        cursor: move;
        color: #333;
    }

    .pvtButton {
        border: 1px solid;
        font-size: 12px;
        margin: 3px;
        transition: 0.34s all cubic-bezier(0.19,1,0.22,1);
        text-decoration: none !important;
    }

    .pvtFilterBox{
        z-index: 100;
        width: 300px;
        border: 1px solid #ccc;
        background-color: #fff;
        position: absolute;
        text-align: center;
        user-select: none;
        min-height: 130px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -khtml-user-select: none;
        -ms-user-select: none;
    }

    .pvtCheckContainer{
        text-align: left;
        font-size: 14px;
        white-space: nowrap;
        overflow-y: scroll;
        width: 100%;
        max-height: 30vh;
        border-top: 1px solid #DFE8F3;
    }

    .pvtCheckContainer p {
        margin: 0;
        margin-bottom: 1px;
        padding: 3px;
        cursor: default;
    }

    .pvtCheckContainer p.selected {
        background: #EBF0F8;
    }

    .pvtOnly {
        display: none;
        width: 35px;
        float: left;
        font-size: 12px;
        padding-left: 5px;
        cursor: pointer;
        text-decoration: none;
    }

    .pvtOnlySpacer {
        display: block;
        width: 35px;
        float: left;
    }

    .pvtCheckContainer p:hover .pvtOnly {
        display: block;
    }
    .pvtCheckContainer p:hover .pvtOnlySpacer {
        display: none;
    }

    .pvtRendererArea { padding: 5px;}
`;