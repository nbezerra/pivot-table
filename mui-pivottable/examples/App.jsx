import React from 'react';
import tips from './tips';
import TableRenderers from '../src/components/TableRenderers';
import createPlotlyComponent from 'react-plotly.js/factory';
import createPlotlyRenderers from '../src/components/PlotlyRenderers';
import PivotTableUI from '../src/PivotTableUI';
import {Box} from '@mui/material';

const Plot = createPlotlyComponent(window.Plotly);

class PivotTableUISmartWrapper extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {pivotState: props};
    }


    componentWillReceiveProps(nextProps) {
        this.setState({pivotState: nextProps});
    }

    render() {
        return <PivotTableUI
            renderers={Object.assign({}, TableRenderers, createPlotlyRenderers(Plot))}
            {...this.state.pivotState} onChange={s => this.setState({pivotState: s})}
            unusedOrientationCutoff={Infinity}
        />;
    }
}

export default class App extends React.Component {
    componentWillMount() {
        this.setState({
            mode: 'demo',
            filename: 'Sample Dataset: Tips',
            pivotState: {
                data: tips
            }
        });
    }

    render() {
        return (
            <Box margin={'40px'}>
                <PivotTableUISmartWrapper {...this.state.pivotState} />
            </Box>);
    }
}
